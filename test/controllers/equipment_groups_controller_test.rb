require 'test_helper'

class EquipmentGroupsControllerTest < ActionController::TestCase
  setup do
    @equipment_group = equipment_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:equipment_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create equipment_group" do
    assert_difference('EquipmentGroup.count') do
      post :create, equipment_group: { name: @equipment_group.name, name_position: @equipment_group.name_position }
    end

    assert_redirected_to equipment_group_path(assigns(:equipment_group))
  end

  test "should show equipment_group" do
    get :show, id: @equipment_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @equipment_group
    assert_response :success
  end

  test "should update equipment_group" do
    patch :update, id: @equipment_group, equipment_group: { name: @equipment_group.name, name_position: @equipment_group.name_position }
    assert_redirected_to equipment_group_path(assigns(:equipment_group))
  end

  test "should destroy equipment_group" do
    assert_difference('EquipmentGroup.count', -1) do
      delete :destroy, id: @equipment_group
    end

    assert_redirected_to equipment_groups_path
  end
end
