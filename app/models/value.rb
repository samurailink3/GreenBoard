# Internal: A Value is one particular data point for a given Measurement. Value
# belongs to one Measurement and LogEntry.
class Value < ActiveRecord::Base
  has_paper_trail
  belongs_to :log_entry
  belongs_to :measurement

  validates :value, presence: true, :unless => Proc.new { |v| v.not_applicable == true}
  validates :tolerance, presence: true, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 1 }
  validates :upper_bound, presence: true
  validates :lower_bound, presence: true
  validates :measurement_id, presence: true

  # Internal: Returns the string or number to be shown in the cell depending on
  # whether or not the Value is not_applicable, boolean, or a number.
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 36, value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 4, created_at: "2014-10-14 15:25:23", updated_at: "2014-10-14 15:25:23">
  #   current_value.value_display
  #   # => 4.0
  #   current_value_2
  #   # => #<Value id: 13, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 2, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_2.value_display
  #   # => "Oh yea"
  #   current_value_3
  #   # => #<Value id: 19, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: true, measurement_id: 7, log_entry_id: 3, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_3.value_display
  #   # => "N/A"
  #
  # Returns a string or number to be show in a table cell.
  def value_display
    if self.not_applicable
      return "N/A"
    else
      if self.measurement.is_bool
        if self.value === 1.0
          return self.true_label
        else
          return self.false_label
        end
      else
        return self.value
      end
    end
  end

  # Internal: Returns the string or number to be shown in the upper bound cell
  # depending on whether or not the Measurement is a boolean or not.
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 36, value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 4, created_at: "2014-10-14 15:25:23", updated_at: "2014-10-14 15:25:23">
  #   current_value.lower_bound_display
  #   # => 1.0
  #   current_value_2
  #   # => #<Value id: 13, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 2, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_2.lower_bound_display
  #   # => "Oh yea"
  #   current_value_3
  #   # => #<Value id: 19, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: true, measurement_id: 7, log_entry_id: 3, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_3.lower_bound_display
  #   # => "Oh yea"
  #
  # Returns a string or number to be show in a table cell.
  def lower_bound_display
    if self.measurement.is_bool
      if self.lower_bound === 1.0
        return self.true_label
      else
        return self.false_label
      end
    else
      return self.lower_bound
    end
  end

  # Internal: Returns the string of number to be shown in the lower bound cell
  # depending on whether or not the Measurement is a boolean or not.
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 36, value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 4, created_at: "2014-10-14 15:25:23", updated_at: "2014-10-14 15:25:23">
  #   current_value.upper_bound_display
  #   # => 5.0
  #   current_value_2
  #   # => #<Value id: 13, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 2, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_2.upper_bound_display
  #   # => "Oh yea"
  #   current_value_3
  #   # => #<Value id: 19, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: true, measurement_id: 7, log_entry_id: 3, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_3.upper_bound_display
  #   # => "Oh yea"
  #
  # Returns a string or number to be show in a table cell.
  def upper_bound_display
    if self.measurement.is_bool
      if self.upper_bound === 1.0
        return self.true_label
      else
        return self.false_label
      end
    else
      return self.upper_bound
    end
  end

  # Internal: Returns a string of metadata of a particular Value. Metadata
  # consists of Lower Bound, Upper Bound, and Tolerance. This is specifically
  # designed to lower user confusion when a particular judgement has been made
  # about a Value (Green/Yellow/Red). This method utilizes lower_bound_display
  # and upper_bound_display .
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 36, value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 4, created_at: "2014-10-14 15:25:23", updated_at: "2014-10-14 15:25:23">
  #   current_value.metadata
  #   # => "Lower Bound: 1.0 | Upper Bound: 5.0 | Tolerance: 0.75"
  #   current_value_2
  #   # => #<Value id: 13, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 2, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_2.metadata
  #   # => "Lower Bound: Oh yea | Upper Bound: Oh yea | Tolerance: 0.0"
  #   current_value_3
  #   # => #<Value id: 19, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: true, measurement_id: 7, log_entry_id: 3, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   current_value_3.metadata
  #   # => "Lower Bound: Oh yea | Upper Bound: Oh yea | Tolerance: 0.0"
  #
  # Returns a string to be displayed when a user hovers over a Green/Yellow/Red
  # table cell.
  def metadata
    return "Lower Bound: " + self.lower_bound_display.to_s + " | " + "Upper Bound: " + self.upper_bound_display.to_s + " | " + "Tolerance: " + self.tolerance.to_s
  end

  # Internal: Returns true if a given Value is up to date. A Value is considered
  # up to date if the Lower Bound, Upper Bound, and Tolerance values are exactly
  # the same as the latest taken Value. If a Value is out of date, a lighter
  # color is shown in the table cell. This is to prevent user confusion if they
  # see a Value that is clearly out of bounds, but that Value was measured
  # against completely different bounds and tolerances.
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 40, value: 56.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 1, log_entry_id: 5, created_at: "2014-10-20 15:29:29", updated_at: "2014-10-20 15:29:29">
  #   current_value.up_to_date
  #   # => false
  #   current_value_2
  #   # => #<Value id: 49, value: 56.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 50.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 1, log_entry_id: 6, created_at: "2014-10-20 15:30:37", updated_at: "2014-10-20 15:30:37">
  #   current_value_2.up_to_date
  #   # => true
  #
  # Returns true or false depending on whether or not a given Value is up to
  # date with the most recent bounds and tolerances.
  def up_to_date
    if ( ( self.upper_bound === self.measurement.values.last.upper_bound ) and ( self.lower_bound === self.measurement.values.last.lower_bound ) and ( self.tolerance === self.measurement.values.last.tolerance ) )
      return true
    else
      return false
    end
  end

end
