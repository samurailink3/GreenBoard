class Authorization < ActiveRecord::Base
  has_paper_trail
  belongs_to :user
  belongs_to :equipment_group

  attr_accessor :username

end
