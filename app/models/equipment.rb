# Internal: A piece of Equipment is an object you want to track various
# Measurement against. Equipment belong to an EquipmentGroup. Equipment have
# many Measurement.
class Equipment < ActiveRecord::Base
  has_paper_trail
  belongs_to :equipment_group
  has_many :measurements, dependent: :destroy

  validates :name, presence: true, length: { maximum: 40 }
  validates :name_position, presence: true

  accepts_nested_attributes_for :measurements, :allow_destroy => true
end
