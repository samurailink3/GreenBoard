class User < ActiveRecord::Base
  has_paper_trail
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :equipment_groups, dependent: :destroy
  has_many :authorizations, dependent: :destroy
  if ENTERPRISE
    before_save :get_ldap_email
    devise :ldap_authenticatable, :rememberable, :trackable
    def get_ldap_email
      self.email = Devise::LDAP::Adapter.get_ldap_param(self.username,"mail").first
    end
  else
    devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  end

  def is_owner(equipment_group)
    if self === equipment_group.owner
      return true
    else
      return false
    end
  end

  def read_only_equipment_groups
    group_array = []
    self.authorizations.each do |a|
      if a.read_permission or a.write_permission
        group_array.push(EquipmentGroup.find(a.equipment_group_id))
      end
    end
    group_array.push(self.equipment_groups)
    return group_array.flatten.uniq.sort! { |a,b| a.name_position <=> b.name_position }
  end

  def write_permission_equipment_groups
    group_array = []
    self.authorizations.each do |a|
      if a.write_permission
        group_array.push(EquipmentGroup.find(a.equipment_group_id))
      end
    end
    group_array.push(self.equipment_groups)
    return group_array.flatten.uniq.sort! { |a,b| a.name_position <=> b.name_position }
  end

  def can_read(equipment_group)
    if read_only_equipment_groups.include? equipment_group
      return true
    else
      return false
    end
  end

  def can_write(equipment_group)
    if write_permission_equipment_groups.include? equipment_group
      return true
    else
      return false
    end
  end

end
