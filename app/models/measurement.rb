# Internal: A Measurement is one single metric you would like to track
# (Temperature, In Motion?, Acidity). Measurement can be numerical or boolean
# (true/false). Measurement belongs to Equipment. Measurement have many Value.
class Measurement < ActiveRecord::Base
  has_paper_trail
  belongs_to :equipment
  has_one :equipment_group, through: :equipment
  has_many :values, dependent: :destroy
end
