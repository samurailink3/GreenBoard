class LogEntry < ActiveRecord::Base
  has_paper_trail
  has_many :values
  accepts_nested_attributes_for :values
end
