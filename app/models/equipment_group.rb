# Internal: A group of related equipment (Paint Line, Brewery Tanks, Chrome
# Platers, etc...). An EquipmentGroup equates to one table. EquipmentGroup
# belongs to a User. EquipmentGroup have many pieces of Equipment.
class EquipmentGroup < ActiveRecord::Base
  has_paper_trail
  belongs_to :owner, class_name: "User", foreign_key: "user_id"
  has_many :equipment, dependent: :destroy
  has_many :authorizations, dependent: :destroy
  has_many :authorized_users, through: :authorizations, :source => :user
  has_many :measurements, through: :equipment

  validates :name, presence: true, length: { maximum: 40 }

  accepts_nested_attributes_for :equipment, :allow_destroy => true
  accepts_nested_attributes_for :measurements, :allow_destroy => true
  accepts_nested_attributes_for :authorizations, :allow_destroy => true

end
