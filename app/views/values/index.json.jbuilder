json.array!(@values) do |value|
  json.extract! value, :id, :value, :tolerance, :upper_bound, :lower_bound, :true_label, :false_label, :not_applicable, :measurement_id, :log_entry_id
  json.url value_url(value, format: :json)
end
