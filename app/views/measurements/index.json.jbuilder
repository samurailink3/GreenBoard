json.array!(@measurements) do |measurement|
  json.extract! measurement, :id, :name, :unit, :name_position, :is_bool, :equipment_id
  json.url measurement_url(measurement, format: :json)
end
