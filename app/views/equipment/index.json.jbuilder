json.array!(@equipment) do |equipment|
  json.extract! equipment, :id, :name, :name_position, :equipment_group_id
  json.url equipment_url(equipment, format: :json)
end
