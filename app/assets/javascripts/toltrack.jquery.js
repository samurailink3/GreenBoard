(function ( $ ) {

  $.fn.validate = function() {
    if ($('table.simple').length>0) {
      // For each row in the table body, do the following:
      $('table.simple > tbody > tr').each(function (index) {

        // Set variables for each row
        lower_bound = $(this).find('td.lower_bound').text();
        upper_bound = $(this).find('td.upper_bound').text();
        tolerance = $(this).find('td.tolerance').text();
        value = $(this).find('td > .value').val();
        icon = $(this).find('td > .verify-icon');
        is_bool = $(this).find('td > .is_bool').text();

        // If the question is a boolean, do this:
        if (is_bool == "Yes"){
          // If it is acceptable, make the icon a green check
          if (is_acceptable(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-remove').addClass('glyphicon-ok').css('color','green');
          // If not, a red X
          } else {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-ok').addClass('glyphicon-remove').css('color','red');
          };
        // If the question is not a boolean, do this:
        } else {
          // If value is null, skip it (extra rows will do this)
          if (isNaN(value)) {
          // Otherwise, check to see if it is a desired value, if so, make the icon a green check
          } else if (is_desired(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-remove').addClass('glyphicon-ok').css('color','green');
          // Otherwise, if it is only acceptable, make the icon an orange warning sign
          } else if (is_acceptable(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-ok glyphicon-remove').addClass('glyphicon-warning-sign').css('color','orange');
          // If the value isn't acceptable at all, make the icon a red X
          } else {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-ok').addClass('glyphicon-remove').css('color','red');
          };
        };
      });
      // Return the object itself so you can use jQuery stringing
      return this;
    } else {
      // For each row in the table body, do the following:
      $('tbody > tr').each(function (index) {

        // Set variables for each row
        lower_bound = $(this).find('td > .lower_bound').val();
        upper_bound = $(this).find('td > .upper_bound').val();
        tolerance = $(this).find('td > .tolerance').val();
        value = $(this).find('td > .value').val();
        icon = $(this).find('td > .verify-icon');
        is_bool = $(this).find('td > .is_bool').text();

        // If the question is a boolean, do this:
        if (is_bool == "Yes"){
          // If it is acceptable, make the icon a green check
          if (is_acceptable(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-remove').addClass('glyphicon-ok').css('color','green');
          // If not, a red X
          } else {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-ok').addClass('glyphicon-remove').css('color','red');
          };
        // If the question is not a boolean, do this:
        } else {
          // If value is null, skip it (extra rows will do this)
          if (isNaN(value)) {
          // Otherwise, check to see if it is a desired value, if so, make the icon a green check
          } else if (is_desired(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-remove').addClass('glyphicon-ok').css('color','green');
          // Otherwise, if it is only acceptable, make the icon an orange warning sign
          } else if (is_acceptable(value, lower_bound, upper_bound, tolerance)) {
            $(icon).removeClass('glyphicon-ok glyphicon-remove').addClass('glyphicon-warning-sign').css('color','orange');
          // If the value isn't acceptable at all, make the icon a red X
          } else {
            $(icon).removeClass('glyphicon-warning-sign glyphicon-ok').addClass('glyphicon-remove').css('color','red');
          };
        };
      });
      // Return the object itself so you can use jQuery stringing
      return this;
    }
  };

  // A mathematical function used to see if a value is in tolerance or falling out of tolerance
  function is_desired(value, lower_bound, upper_bound, tolerance) {
    return value >= ( lower_bound + ( tolerance * ( upper_bound - lower_bound ) ) ) && value <= ( upper_bound - ( tolerance * ( upper_bound - lower_bound ) ) )
  }

  // A mathematical function used to see if a value is purely good or bad
  function is_acceptable(value, lower_bound, upper_bound, tolerance) {
    return value >= lower_bound && value <= upper_bound
  }

}( jQuery ));