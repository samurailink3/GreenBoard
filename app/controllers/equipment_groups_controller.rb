class EquipmentGroupsController < ApplicationController
  before_action :set_equipment_group, only: [:show, :edit, :update, :destroy, :table, :just_the_table, :tv_mode, :new_row, :settings]
  before_filter :check_read_permission, except: [:new, :create, :index]
  before_filter :authenticate, except: [:table, :just_the_table, :tv_mode]

  # GET /equipment_groups
  # GET /equipment_groups.json
  def index
  end

  # GET /equipment_groups/1
  # GET /equipment_groups/1.json
  def show
  end

  # GET /equipment_groups/new
  def new
    @equipment_group = EquipmentGroup.new
    @equipment_group.public_access = false
  end

  # GET /equipment_groups/1/edit
  def edit
  end

  # POST /equipment_groups
  # POST /equipment_groups.json
  def create
    @equipment_group = EquipmentGroup.new(equipment_group_params)
    @equipment_group.owner = current_user
    @equipment_group.name_position = Time.now.to_i
    @equipment_group.public_access = false

    respond_to do |format|
      if @equipment_group.save
        format.html { redirect_to equipment_group_table_path(@equipment_group), notice: 'Equipment group was successfully created.' }
        format.json { render :show, status: :created, location: @equipment_group }
      else
        format.html { render :new }
        format.json { render json: @equipment_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipment_groups/1
  # PATCH/PUT /equipment_groups/1.json
  def update
    respond_to do |format|
      if @equipment_group.update(equipment_group_params)
        format.html { redirect_to equipment_group_table_path(@equipment_group), notice: 'Equipment group was successfully updated.' }
        format.json { render :show, status: :ok, location: @equipment_group }
      else
        format.html { render :edit }
        format.json { render json: @equipment_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment_groups/1
  # DELETE /equipment_groups/1.json
  def destroy
    @equipment_group.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Equipment group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def table
    authenticate unless @equipment_group.public_access
    @equipment = @equipment_group.equipment
  end

  def just_the_table
    authenticate unless @equipment_group.public_access
    @equipment = @equipment_group.equipment
    render :layout => "blank"
  end

  def tv_mode
    authenticate unless @equipment_group.public_access
    @equipment = @equipment_group.equipment
    render :layout => "blank"
  end

  def settings
    if current_user.is_owner(@equipment_group)
    else
      redirect_to root_url, :alert => "You don't have access to this area."
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment_group
      @equipment_group = EquipmentGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def equipment_group_params
      params.require(:equipment_group).permit(:name, :name_position, :public_access, :user_id, equipment_attributes: [:id, :name, :name_position, :equipment_group_id, :_destroy], authorizations_attributes: [:id, :user_id, :equipment_group_id, :read_permission, :write_permission, :_destroy, :username])
    end

    def check_read_permission
      if @equipment_group.public_access
      else
        if user_signed_in?
          if current_user.is_owner(@equipment_group) or current_user.can_read(@equipment_group)
          else
            redirect_to root_url, :alert => "You don't have permission to see this."
          end
        else
          redirect_to root_url, :alert => "You don't have permission to see this."
        end
      end
    end
end
