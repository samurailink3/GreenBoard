class LogEntriesController < ApplicationController
  before_action :set_equipment_group, only: [:new_row, :new_row_simple]
  before_filter :authenticate

  # POST /log_entries
  # POST /log_entries.json
  def create
    @log_entry = LogEntry.new(log_entry_params)

    respond_to do |format|
      if @log_entry.save
        format.html { redirect_to equipment_group_table_path(EquipmentGroup.find(Equipment.find(Measurement.find(@_params[:log_entry][:values_attributes]["0"][:measurement_id]).equipment_id).equipment_group_id)), notice: 'Log entry was successfully created.' }
        format.json { render :show, status: :created, location: @log_entry }
      else
        format.html {
          if @log_entry.errors.any?
            flash[:alert] = @log_entry.errors.full_messages.join(". ")
          end
          redirect_to new_row_path(EquipmentGroup.find(Equipment.find(Measurement.find(@_params[:log_entry][:values_attributes]["0"][:measurement_id]).equipment_id).equipment_group_id))
        }
        format.json { render json: @log_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  def new_row
    if current_user.is_owner(@equipment_group) or current_user.can_write(@equipment_group)
    else
      redirect_to equipment_group_table_path(@equipment_group), :alert => "You can't write here (no permission)."
    end
    @log_entry = LogEntry.new
  end

  def new_row_simple
    if current_user.is_owner(@equipment_group) or current_user.can_write(@equipment_group)
    else
      redirect_to equipment_group_table_path(@equipment_group), :alert => "You can't write here (no permission)."
    end
    @log_entry = LogEntry.new
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log_entry
      @log_entry = LogEntry.find(params[:id])
    end

    def set_equipment_group
      @equipment_group = EquipmentGroup.find(params[:id])
      @equipment = @equipment_group.equipment
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def log_entry_params
      params.require(:log_entry).permit(:capture_date, values_attributes: [:id, :value, :tolerance, :upper_bound, :lower_bound, :true_label, :false_label, :not_applicable, :measurement_id, :log_entry_id, :_destroy])
    end
end
