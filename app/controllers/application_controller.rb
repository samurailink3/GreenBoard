# Internal: The main controller for GreenBoard.
class ApplicationController < ActionController::Base
  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :validation_color
  before_action :set_equipment_groups

  before_filter :configure_permitted_parameters, if: :devise_controller?

  # Internal: Configure Devise (Use login system) to accept usernames in
  # addition to email addresses. No examples are needed, as this is a
  # configuration option.
  #
  # Returns nothing. Only used to configure Devise.
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation) }
  end

  # Internal: Check to see if a User is logged in, if not, redirect to
  # the home page and display a notice.
  #
  # Examples
  #
  #   before_filter :authenticate
  #   # If user is logged in, the controller proceeds to load the page
  #   # If the user is not logged in, redirect the home page with a notice
  #
  # Returns nothing. Only used to validate authentication.
  def authenticate
    if current_user
    else
      redirect_to root_url, :notice => "You must log in to do that."
    end
  end

  # Internal: If a User is logged in, set @equipment_groups to the list of
  # EquipmentGroup they can read/write from/to. If the User is not logged in,
  # return an empty list of EquipmentGroup. This keeps the main menu from
  # breaking if a User is not logged in.
  #
  # Examples
  #
  #   before_action :set_equipment_groups
  #   # If user is logged in:
  #   @equipment_groups
  #   # => [#<EquipmentGroup id: 1, name: "Brewery Tanks!", name_position: 1407415605, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34", user_id: 1, public_access: false>]
  #   # If user is not logged in:
  #   @equipment_groups
  #   # => nil
  #
  # Returns nothing, only sets the @equipment_groups variable.
  def set_equipment_groups
    if current_user
      @equipment_groups = current_user.read_only_equipment_groups
    else
      @equipment_groups = nil
    end
  end

  # Internal: Reports if a Value is acceptable or not (also known as the
  # Red/Green test). This tests whether a value is within the upper and lower
  # bounds.
  #
  # current_value - The Value to be evaluated. Contains (at least) the following attributes:
  #                 current_value.value       - The current collected value of
  #                                             a given measurement.
  #                 current_value.lower_bound - The current lower bound value of
  #                                             a given measurement
  #                 current_value.upper_bound - The current upper bound value of
  #                                             a given measurement
  #
  # Examples
  #
  #   current_value
  #   #  => #<Value id: 1, value: 1.5, tolerance: 0.8, upper_bound: 2.0, lower_bound: 1.5, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 4, log_entry_id: 1, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   is_acceptable(current_value)
  #   #  => true
  #
  # Returns true or false depending on whether the Value is acceptable or not.
  def is_acceptable(current_value)
    if current_value.value >= current_value.lower_bound and current_value.value <= current_value.upper_bound
      return true
    else
      return false
    end
  end

  # Internal: Reports if a Value is within tolerance bounds, or is falling out
  # of tolerance (known as the Green/Yellow test). This tests whether a Value is
  # within the current tolerance percentages relating to upper and lower bounds.
  #
  # current_value - The Value to be evaluated. Contains (at least) the following attributes:
  #                 current_value.value       - The current collected value of
  #                                             a given measurement.
  #                 current_value.lower_bound - The current lower bound value of
  #                                             a given measurement
  #                 current_value.upper_bound - The current upper bound value of
  #                                             a given measurement
  #                 current_value.tolerance   - The current tolerance of a given
  #                                             measurement. This should be a
  #                                             number between 0 and 1, as it
  #                                             needs to be a percentage.
  #
  # Examples
  #
  #   current_value
  #   #  => #<Value id: 1, value: 1.5, tolerance: 0.8, upper_bound: 2.0, lower_bound: 1.5, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 4, log_entry_id: 1, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   is_desired(current_value)
  #   #  => false
  #   current_value_2
  #   # => #<Value id: 4, value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 1, created_at: "2014-10-14 15:03:34", updated_at: "2014-10-14 15:03:34">
  #   is_desired(curent_value_2)
  #   # => true
  #
  # Returns true or false depending on whether the Value is desired (in tolerance) or not (falling out of tolerance).
  def is_desired(current_value)
    if current_value.value >= ( current_value.lower_bound + ( current_value.tolerance * ( current_value.upper_bound - current_value.lower_bound ) ) ) and current_value.value <= ( current_value.upper_bound - ( current_value.tolerance * ( current_value.upper_bound - current_value.lower_bound ) ) )
      return true
    else
      return false
    end
  end

  # Internal: Returns the color the cell should be based on the current Value.
  # This string is used in the view as a class name and CSS colors the cell
  # appropriately. This method uses is_acceptable and is_desired to determine
  # the return Value. If a Value is up_to_date, the cell will get the full
  # color, otherwise, a lighter color is returned (See Value for more
  # information on up_to_date). If the Value is marked as not_applicable, the
  # color will be blue and all labels changed to "N/A".
  #
  # current_value - The Value to be evaluated. Contains (at least) the following attributes:
  #                 current_value.not_applicable - Marks a value as
  #                                                "Not Applicable". This has
  #                                                the effect of skipping all
  #                                                validation efforts and
  #                                                coloring the cell blue.
  #
  # Examples
  #
  #   current_value
  #   # => #<Value id: 36, value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 4, created_at: "2014-10-14 15:25:23", updated_at: "2014-10-14 15:25:23">
  #   validation_color(current_value)
  #   # => "yellow-bg"
  #
  # Returns a string with the validation color class name.
  def validation_color(current_value)
    if current_value.not_applicable
      current_value.value = "N/A"
      current_value.true_label = "N/A"
      current_value.false_label = "N/A"
      return "blue-bg"
    elsif is_desired(current_value)
      if current_value.up_to_date
        return "green-bg"
      else
        return "light-green-bg"
      end
    elsif is_acceptable(current_value)
      if current_value.up_to_date
        return "yellow-bg"
      else
        return "light-yellow-bg"
      end
    else
      if current_value.up_to_date
        return "red-bg"
      else
        return "light-red-bg"
      end
    end
  end

end
