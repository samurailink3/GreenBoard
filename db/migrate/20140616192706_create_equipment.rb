class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :name
      t.integer :name_position
      t.integer :equipment_group_id

      t.timestamps
    end
  end
end
