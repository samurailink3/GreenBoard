class CreateLogEntries < ActiveRecord::Migration
  def change
    create_table :log_entries do |t|
      t.datetime :capture_date

      t.timestamps
    end
  end
end
