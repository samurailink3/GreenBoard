class CreateMeasurements < ActiveRecord::Migration
  def change
    create_table :measurements do |t|
      t.string :name
      t.string :unit
      t.integer :name_position
      t.boolean :is_bool
      t.integer :equipment_id

      t.timestamps
    end
  end
end
