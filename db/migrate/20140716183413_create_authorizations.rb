class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|
      t.integer :user_id
      t.integer :equipment_group_id
      t.boolean :read_permission
      t.boolean :write_permission

      t.timestamps
    end
  end
end
