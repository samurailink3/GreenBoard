class CreateEquipmentGroups < ActiveRecord::Migration
  def change
    create_table :equipment_groups do |t|
      t.string :name
      t.integer :name_position

      t.timestamps
    end
  end
end
