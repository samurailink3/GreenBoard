class AddPublicAccessToEquipmentGroup < ActiveRecord::Migration
  def change
    add_column :equipment_groups, :public_access, :boolean
  end
end
