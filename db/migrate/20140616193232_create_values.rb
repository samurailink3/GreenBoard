class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.float :value
      t.float :tolerance
      t.float :upper_bound
      t.float :lower_bound
      t.string :true_label
      t.string :false_label
      t.boolean :not_applicable
      t.integer :measurement_id
      t.integer :log_entry_id

      t.timestamps
    end
  end
end
