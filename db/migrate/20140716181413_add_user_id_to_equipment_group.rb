class AddUserIdToEquipmentGroup < ActiveRecord::Migration
  def change
    add_column :equipment_groups, :user_id, :integer
  end
end
