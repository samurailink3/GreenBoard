User.create!([
  {email: "samurailink3@gmail.com", password: "password", encrypted_password: "$2a$10$8nFCH6KRJ9caT0nCe1gaDOBjetokuU5U7mYUmY2HKAwPzEaHPuGp6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2014-08-07 12:44:56", last_sign_in_at: "2014-08-07 12:44:56", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", username: "samurailink3"},
  {email: "billy@example.com", password: "password", encrypted_password: "$2a$10$8nFCH6KRJ9caT0nCe1gaDOBjetokuU5U7mYUmY2HKAwPzEaHPuGp6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2014-08-07 12:44:56", last_sign_in_at: "2014-08-07 12:44:56", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", username: "billy"},
  {email: "bobby@example.com", password: "password", encrypted_password: "$2a$10$8nFCH6KRJ9caT0nCe1gaDOBjetokuU5U7mYUmY2HKAwPzEaHPuGp6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2014-08-07 12:44:56", last_sign_in_at: "2014-08-07 12:44:56", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", username: "bobby"},
  {email: "johnny@example.com", password: "password", encrypted_password: "$2a$10$8nFCH6KRJ9caT0nCe1gaDOBjetokuU5U7mYUmY2HKAwPzEaHPuGp6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2014-08-07 12:44:56", last_sign_in_at: "2014-08-07 12:44:56", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1", username: "johnny"}
])
Authorization.create!([
  {user_id: 2, equipment_group_id: 1, read_permission: true, write_permission: true},
  {user_id: 3, equipment_group_id: 1, read_permission: true, write_permission: false}
])
Equipment.create!([
  {name: "Milk Stout", name_position: 2, equipment_group_id: 1},
  {name: "Dutch Apple Ale", name_position: 3, equipment_group_id: 1},
  {name: "Lemon Wheat", name_position: 1, equipment_group_id: 1}
])
EquipmentGroup.create!([
  {name: "Brewery Tanks!", name_position: 1407415605, user_id: 1, public_access: false}
])
LogEntry.create!([
  {capture_date: "2014-08-07 13:26:15"},
  {capture_date: "2014-08-07 13:31:41"},
  {capture_date: "2014-08-07 13:33:04"}
])
Measurement.create!([
  {name: "Milk", unit: "Percentage", name_position: 1, is_bool: nil, equipment_id: 1},
  {name: "Stout", unit: "Percentage", name_position: 2, is_bool: nil, equipment_id: 1},
  {name: "Temperature", unit: "Degress (F)", name_position: 3, is_bool: nil, equipment_id: 1},
  {name: "Apple Juice", unit: "Gallons", name_position: 1, is_bool: nil, equipment_id: 2},
  {name: "Cinnamon", unit: "Teaspoons", name_position: 3, is_bool: nil, equipment_id: 2},
  {name: "Ale Mixture", unit: "Gallons", name_position: 2, is_bool: nil, equipment_id: 2},
  {name: "Lemony?", unit: "Yes/No", name_position: 1, is_bool: true, equipment_id: 3},
  {name: "Wheaty?", unit: "Yes/No", name_position: 2, is_bool: true, equipment_id: 3},
  {name: "Tasty?", unit: "Yes/No", name_position: 3, is_bool: true, equipment_id: 3}
])
Value.create!([
  {value: 1.5, tolerance: 0.8, upper_bound: 2.0, lower_bound: 1.5, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 4, log_entry_id: 1},
  {value: 1.5, tolerance: 0.8, upper_bound: 1.5, lower_bound: 1.0, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 6, log_entry_id: 1},
  {value: 2.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 5, log_entry_id: 1},
  {value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 1},
  {value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Mhmm", false_label: "Ehh", not_applicable: false, measurement_id: 8, log_entry_id: 1},
  {value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "WOO", false_label: "EUGH", not_applicable: false, measurement_id: 9, log_entry_id: 1},
  {value: 50.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 1, log_entry_id: 1},
  {value: 50.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 2, log_entry_id: 1},
  {value: 58.0, tolerance: 0.15, upper_bound: 61.0, lower_bound: 55.0, true_label: nil, false_label: nil, not_applicable: false, measurement_id: 3, log_entry_id: 1},
  {value: 1.6, tolerance: 0.8, upper_bound: 2.0, lower_bound: 1.5, true_label: "", false_label: "", not_applicable: false, measurement_id: 4, log_entry_id: 2},
  {value: 1.4, tolerance: 0.8, upper_bound: 1.5, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 6, log_entry_id: 2},
  {value: 3.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 2},
  {value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: false, measurement_id: 7, log_entry_id: 2},
  {value: 0.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Mhmm", false_label: "Ehh", not_applicable: false, measurement_id: 8, log_entry_id: 2},
  {value: 0.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "WOO", false_label: "EUGH", not_applicable: false, measurement_id: 9, log_entry_id: 2},
  {value: 52.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 1, log_entry_id: 2},
  {value: 48.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 2, log_entry_id: 2},
  {value: 59.0, tolerance: 0.15, upper_bound: 61.0, lower_bound: 55.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 3, log_entry_id: 2},
  {value: 1.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Oh yea", false_label: "Naa", not_applicable: true, measurement_id: 7, log_entry_id: 3},
  {value: 0.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "Mhmm", false_label: "Ehh", not_applicable: true, measurement_id: 8, log_entry_id: 3},
  {value: 0.0, tolerance: 0.0, upper_bound: 1.0, lower_bound: 1.0, true_label: "WOO", false_label: "EUGH", not_applicable: true, measurement_id: 9, log_entry_id: 3},
  {value: 55.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 1, log_entry_id: 3},
  {value: 45.0, tolerance: 0.25, upper_bound: 60.0, lower_bound: 40.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 2, log_entry_id: 3},
  {value: 53.0, tolerance: 0.15, upper_bound: 61.0, lower_bound: 55.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 3, log_entry_id: 3},
  {value: 2.0, tolerance: 0.8, upper_bound: 2.0, lower_bound: 1.5, true_label: "", false_label: "", not_applicable: false, measurement_id: 4, log_entry_id: 3},
  {value: 1.0, tolerance: 0.8, upper_bound: 1.5, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 6, log_entry_id: 3},
  {value: 4.0, tolerance: 0.75, upper_bound: 5.0, lower_bound: 1.0, true_label: "", false_label: "", not_applicable: false, measurement_id: 5, log_entry_id: 3}
])
