listen "127.0.0.1:3002"
worker_processes 2
pid "tmp/pids/unicorn.pid"
stderr_path "log/unicorn/error.log"
stdout_path "log/unicorn/standard.log"
