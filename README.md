GreenBoard
==========

The simple way to track and visualize status and statistics.

## Issues and Feature Requests

The best way to request features or report bugs is by using [GitHub
Issues](https://github.com/samurailink3/greenboard/issues/). Support is
best-case, as I'm pretty busy recently.

## Contributing

Pull requests are very much welcome! Try to keep history succinct, rebasing onto
a single commit (unless there's good reason to spread things out) is very much
appreciated. Using feature branches for your work gets you a gold star.

### Branches

**master** - This will always be considered "Production Ready" and stable. On a
further note,
[tags/releases](https://github.com/samurailink3/GreenBoard/releases) will
denote specific version numbers.

**develop** - Any work in progress that isn't quite stable should be here. On
good days, consider this the 'beta' channel.

**feature-* branches** - Any feature work, this may or may not be stable, will
most likely get rebased onto a single merge commit. Consider this complete
unstable with changing history. Forking from a feature branch is probably a
bad idea in most cases.

### Licensing

We're using the [MIT License](http://opensource.org/licenses/MIT), any
contributions will be under the main project license. Mixed licensing content
makes this very very complicated, let's not do that.

## Deployment

If you're familiar with Rails applications and how to deploy them, there's
nothing special here. Just make sure to fill out and rename the following files:

* `config/database.yml.example`
* `config/deploy.rb.example`
* `config/ldap.yml.example`
* `config/deploy/production.rb.example`
* `config/initializers/config.rb.example`

Capistrano is installed, though you should tailor it for your environment.

Eventually I'll be moving the configuration to environment variables, courtesy
of [Figaro](https://github.com/laserlemon/figaro).

### Example Manual Deployment: Debian (Wheezy) and Apache

This deployment is for one single server, no capistrano, nothing fancy or automated.

If you aren't familiar with deploying a rails application, here's an example
using Debian Wheezy, Unicorn, and Apache. Make sure you have the web server Debian role
selected.

* Become root - `sudo su - root`
* First install build-essential, Postgres, and some dev libraries - `apt-get install build-essential postgresql postgresql-client libpq-dev sqlite libsqlite3-dev`
* Next, download, compile, and install Ruby 2.1.3
  * `wget http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.3.tar.gz`
  * `tar xf ruby-2.1.3.tar.gz`
  * `cd ruby-2.1.3/`
  * `./configure`
  * `make`
  * `make install`
  * Log out
  * Log back in
* Create a new user for deployment - `adduser greenboard --home /opt/greenboard`
  * Choose a long, random password and store in your password tool of choice.
* Add user to passwordless-sudo group - `adduser greenboard sudo`
* Time to set up the database, log in as Postgres - `su - postgres`
* Create a database user - `createuser greenboard`
  * Superuser - No
  * Create databases - No
  * Create roles - No
* Create a database - `createdb greenboard-production --owner=greenboard --password`
  * Choose a long, random password and store in your password tool of choice.
* Log out of postgres - `exit`
* Log in as greenboard - `su - greenboard`
* Clone the repository - `git clone --bare https://github.com/samurailink3/greenboard .git`
* Turn off bare repo mode - `git config core.bare false`
* Populate most recent master branch - `git reset --hard master`
* Copy config files, then fill them out
  * `cp config/database.yml.example config/database.yml`
  * `cp config/ldap.yml.example config/ldap.yml`
  * `cp config/initializers/config.rb.example config/initializers/config.rb`
* Install bundler - `sudo gem install bundler`
* Install application gems - `RAILS_ENV=production bundle install --path vendor/bundle`
* Set up the database - `RAILS_ENV=production bundle exec rake db:migrate`
* Precompile the assets - `bundle exec rake assets:precompile`
* Create unicorn log directories - `mkdir log/unicorn`
* Copy your init script - `sudo cp -f vendor/greenboard /etc/init.d/greenboard`
* Make sure it's executable `sudo chmod +x /etc/init.d/greenboard`
* Copy apache site configuration - `sudo cp -f vendor/greenboard-apache.conf /etc/apache2/sites-available/greenboard-apache`
* Configure the site URL (ServerName) - `sudo nano /etc/apache2/sites-available/greenboard-apache`
* Enable the site - `sudo a2ensite greenboard-apache`
* Enable the proxy modules - `sudo a2enmod proxy proxy_balancer proxy_http`
* Restart apache - `sudo service apache2 restart`
* Start GreenBoard - `sudo service greenboard start`

#### Updating

If you've deployed with the strategy above, you can update with these simple commands:

* `sudo su - greenboard`
* `git pull origin master`
* `sudo service greenboard restart`

## Sponsors

Much of this application has been sponsored by [BWI
Group](http://www.bwigroup.com/en/). Much thanks goes out to the company for
financing and consulting during development.
