#!/bin/bash

rake db:drop db:create db:migrate db:seed assets:precompile RAILS_ENV=production

unicorn -p 3000 -E production